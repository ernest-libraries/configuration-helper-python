import configparser


js = {'Ernest': {'Money': 500, 'Age': 22}, 'Gai': {'Gender': 'Male'}, 'Kay': 'I am the king.'}

config = configparser.ConfigParser()

for j in js.keys():
    print(j)

# https://www.kite.com/python/answers/how-to-read-a-dictionary-from-a-file-in--python


def strategy_dict(strategy_code, ernest_ratio, sharpe_ratio,
                  sortino_ratio, calmar_ratio, maxi_drawdown,
                  annual_return, frequency, conditions):
    return {
        'strategy_code': strategy_code,
        'ernest_ratio': ernest_ratio,
        'sharpe_ratio': sharpe_ratio,
        'sortino_ratio': sortino_ratio,
        'calmar_ratio': calmar_ratio,
        'maxi_drawdown': maxi_drawdown,
        'annual_return': annual_return,
        'frequency': frequency,
        'conditions': conditions
    }
